const express = require('express');
const axios = require('axios');
const { JSDOM } = require('jsdom');
const { Readability } = require('@mozilla/readability');
const TurndownService = require('turndown');
const fs = require('fs');
const markdownIt = require('markdown-it');

const app = express();
const port = 3000;

app.use(express.static('public'));

async function downloadAndExtractContent(url) {
    try {
        const response = await axios.get(url);
        const dom = new JSDOM(response.data, {
            url: url,
        });
        const reader = new Readability(dom.window.document);
        const article = reader.parse();

        return article.content;
    } catch (error) {
        console.log('Error:', error);
        throw error;
    }
}

async function convertHtmlToMarkdown(html_data) {
    const turndownService = new TurndownService();
    const markdown = turndownService.turndown(html_data);

    return markdown;
}

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/convert', async (req, res) => {
    const url = req.query.url;
    if (!url) {
        return res.send('URL이 필요합니다.');
    }

    try {
        const htmlString = await downloadAndExtractContent(url);
        const markdown = await convertHtmlToMarkdown(htmlString);

        fs.writeFileSync('./output.md', markdown, 'utf8');
        const mdFile = fs.readFileSync('./output.md', 'utf8');

        const md = markdownIt();
        const renderedHtml = md.render(mdFile);

        res.send(renderedHtml);
    } catch (error) {
        res.send(`오류가 발생했습니다: ${error.message}`);
    }
});

app.listen(port, () => {
    console.log(`[Server] :: Listening on Port ${port}`)
});
